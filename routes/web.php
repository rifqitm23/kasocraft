<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/home', 'HomeController@index')->name('home');

// Authentikasi
Auth::routes();

// Home	
Route::get('/', function () {
	$kategori = App\Kategori::all();
	return view('home', [
		'kategori' => $kategori,
	]);
})->name('home');

// Pencarian
Route::get('/q', 'ProdukController@search');

// Cara Pesan
Route::get('/cara', function () {
	return view('carapesan');
})->name('carapesan');

// Tentang
Route::get('/tentang', function () {
	return view('about');
})->name('tentang');

// Halaman Admin
Route::prefix('admin')->group(function() {

	Route::get('/', 'AdminController@dashboard')->name('admin');

	Route::post('/tambahbarang', 'AdminController@storebarang');

	// Halaman Admin>Barang
	Route::prefix('barang')->group(function() {
		// C
		Route::post('/tambah', 'AdminController@storebarang');
		// R
		Route::get('/', 'AdminController@barang')->name('barang');
		Route::get('/{barang}', 'AdminController@showbarang');
		// U
		Route::get('/edit/{barang}', 'AdminController@showEditBarang');
		Route::put('/edit/{barang}', 'AdminController@updatebarang');
		// D
		Route::delete('/delete/{barang}', 'AdminController@destroybarang');
	});

	// Halaman Admin>Transaksi
	Route::prefix('transaksi')->group(function() {

		Route::get('/', 'AdminController@dashboard')->name('transaksi');
		Route::get('/{transaksi}', 'AdminController@showTransaksi');
		Route::put('/{transaksi}', 'AdminController@eksekusiTransaksi');
	});
});

// Halaman Produk
Route::prefix('produk')->group(function() {

	Route::get('/', 'ProdukController@index')->name('produk');
	Route::get('/detail/{barang}', 'ProdukController@show');
	Route::get('/kategori/{kategori}', 'ProdukController@kategori');
});

// Halaman Troli
Route::prefix('troli')->group(function() {

	Route::get('/', 'TroliController@troliku')->name('troli');
	Route::post('/asdfgh/{barang}', 'TroliController@masukkanTroli')->name('addtocart');
	Route::post('/bayar/{troli}', 'TroliController@bayar');
	Route::put('/asdfgh/{troli}/{barang}', 'TroliController@editQty');
	Route::delete('/asdfgh/{troli}/{id}', 'TroliController@hapusBarangTroli');
});

Route::prefix('transaksi')->group(function() {

	Route::get('/', 'TransaksiController@transaksiku')->name('transaksiku');
	Route::get('/{transaksi}', 'TransaksiController@show');	
	Route::post('/konfirmasi/{transaksi}', 'TransaksiController@konfirm');
});
// use App\Troli;
// Route::get('coba/{transaksi}', function (Troli $transaksi) {
// 	return new App\Mail\Tagihan($transaksi);
// });
