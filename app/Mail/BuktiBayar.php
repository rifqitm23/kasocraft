<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Troli;

class BuktiBayar extends Mailable
{
    use Queueable, SerializesModels;

    public $troli;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Troli $troli)
    {
        $this->troli = $troli;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.buktiBayar')
                    ->with(['troli' => $this->troli])
                    ->attach('img/bukti/'.$this->troli->bukti->bukti_image);
    }
}
