<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Troli;

class TransaksiOk extends Mailable
{
    use Queueable, SerializesModels;

    public $troli;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Troli $troli)
    {
        $this->troli = $troli;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.transaksiOk')
                    ->with([
                        'barangs' => $this->troli->barangs()->get() ,
                        'troli' => $this->troli
                    ]);
    }
}
