<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Barang;
class Kategori extends Model
{
	protected $table = 'kategori';

	public function getRouteKeyName()
	{
		return 'slug';
	}

	public function barangs()
	{
		return $this->hasMany(Barang::class);
	}
}
