<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Barang;
use App\BuktiBayar;
use App\Status;

class Troli extends Model
{
    protected $table = 'troli';

    protected $fillable = [
    	'status_id', 'keterangan'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function barangs()
    {
        return $this->belongsToMany(Barang::class, 'troli_barang')->withPivot('qty');
    }

    public function bukti()
    {
        return $this->hasOne(BuktiBayar::class, 'troli_id');
    }
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function harga()
    {
        $barangs = $this->barangs()->get();
        $total = 0;
        foreach ($barangs as $barang) {
            $total += $barang->harga * $barang->pivot->qty;
        }
        return $total;
    }
    public function totalHarga()
    {
        $barangs = $this->barangs()->get();
        $total = 0;
        foreach ($barangs as $barang) {
            $total += $barang->harga * $barang->pivot->qty;
        }

        $total += $this->biayaOngkir();

        return "Rp ". number_format($total,2,',','.');
    }

    public function totalBerat()
    {
        $barangs = $this->barangs()->get();
        $total = 0;
        foreach ($barangs as $barang) {
            $total += $barang->gram * $barang->pivot->qty;
        }
        $total = (float) $total / 1000;
        return $total . " kg";
    }

    public function biayaOngkir()
    {
        $barangs = $this->barangs()->get();
        $total = 0;
        foreach ($barangs as $barang) {
            $total += $barang->gram * $barang->pivot->qty;
        }
        $total = (float) $total / 1000;

        return ceil($total) * 0;
    }
}