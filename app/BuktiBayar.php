<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Troli;

class BuktiBayar extends Model
{
    protected $table = 'buktibayar';

    protected $fillable = [
    	'nama','nominal','bukti_image'
    ];

    public function transaksi()
    {
    	return $this->belongsTo(Troli::class, 'troli_id');
    }
}
