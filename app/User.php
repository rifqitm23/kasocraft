<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Troli;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama', 'email', 'password', 'no_telp', 'alamat'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function troli()
    {
        return $this->hasMany(Troli::class);
    }

    public function activeTroli()
    {
        return $this->troli()->firstOrCreate(['status_id' => 1 ]);
    }

    public function getTroliBelumTerbayar()
    {   
        return $this->troli()->where('status_id', 2)->get();
    }

}
