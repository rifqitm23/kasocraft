<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Kategori;
use App\Troli;

class Barang extends Model
{
    
    protected $fillable = [
        'nama', 'deskripsi', 'kategori_id', 'gram', 'stok', 'harga', 'nama_gambar'
    ];
    
    public function kategori()
    {
    	return $this->belongsTo(Kategori::class);
    }

    public function troli()
    {
        return $this->belongsToMany(Troli::class, 'troli_barang')->withPivot('qty');
    }

    public function hargarupiah()
    {
        return "Rp ". number_format($this->harga,2,',','.');
    }

    public function beratkg()
    {
        return ''.((float) $this->gram / 1000).' kg';
    }

    public function gambar()
    {
        return asset('img/produk/'.$this->nama_gambar);
    }

    public function totalRp()
    {
        return "Rp ". number_format($this->harga * $this->pivot->qty, 2, ',', '.');
    }

}
