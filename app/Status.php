<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Troli;

class Status extends Model
{
	protected $table = 'status';
    public function troli()
    {
    	return $this->hasMany(Troli::class);    }
}
