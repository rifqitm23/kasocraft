<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\Troli;
use DB;
use App\Mail\Tagihan;
use Mail;
use Carbon\Carbon;

class TroliController extends Controller
{
    //
    public function __construct()
    {
    	$this->middleware('auth');
    }

    private function attachOrUpdateBarang(Troli $troli, Barang $barang, $jumlah)
    {
    	if  (DB::table('troli_barang')
            ->where('troli_id', $troli->id)
            ->where('barang_id', $barang->id)
            ->count() > 0)
    	{
    		DB::table('troli_barang')
    			->where('troli_id', $troli->id)
            	->where('barang_id', $barang->id)
            	->increment('qty', $jumlah);
    	} else {
    		$troli->barangs()->attach($barang->id, ['qty' => $jumlah]);
    	}
    }

    public function masukkanTroli(Request $request, Barang $barang)
    {
        if (count($request->user()->getTroliBelumTerbayar()) > 3) {
            return redirect('/transaksiku');
        }
    	$request->validate([
    		'jumlah' => 'required|numeric|min:1'
    	]);

    	$troli = $request->user()->activeTroli();

    	$this->attachOrUpdateBarang($troli, $barang, $request->jumlah);

    	return redirect('/produk/detail/'.$barang->id)->with('sukses', 'Barang berhasil ditambahkan di troli');
    }

    public function editQty(Request $request, Troli $troli, Barang $barang)
    {
        if ($request->qty <= $barang->stok)
            $troli->barangs()->updateExistingPivot($barang->id, ['qty' => $request->qty]);

        return redirect()->back();
    }

    public function hapusBarangTroli(Request $request,Troli $troli, $id)
    {
        $troli->barangs()->detach($id);
        return redirect()->back();
    }

    public function Troliku(Request $request)
    {
        $troli = $request->user()->activeTroli();
    	$barangs = $troli->barangs()->get();
    	$total = 0;
    	foreach ($barangs as $barang) {
    		$total += $barang->harga * $barang->pivot->qty;
    	}
    	$total = "Rp ". number_format($total,2,',','.');
    	return view('troliku', [
    		'barang_orderan' => $barangs,
    		'total'	=> $total,
            'idTroli' => $troli->id, 
    	]);
    }

    public function bayar(Request $request, Troli $troli)
    {
        $barangs = $troli->barangs()->get();
        if (count($barangs) <= 0) {
            return redirect()->route('troli')
                    ->with('gagal', 'Silahkan membeli produk kami terlebih dahulu.');
        }

        $sukses = TRUE;
        $message = [];
        foreach ($barangs as $barang) {
            if ($barang->stok == 0) {
                $troli->barangs()->detach($barang->id);
                $sukses = FALSE;
            }
            if ($barang->stok < $barang->pivot->qty) {
                $barang->pivot->qty = $barang->stok;
                $barang->pivot->save();
                $sukses = FALSE;
            }
        }

        if ($sukses) 
        {
            $troli->status_id = 2;
            $troli->tgl_beli = Carbon::now()->format('Y-m-d H:i:s');
            foreach ($barangs as $barang) {
                $barang->stok = $barang->stok - $barang->pivot->qty;
                $barang->save();
            }
            $troli->save();
            $email = $request->user()->email;
            $nama = $request->user()->nama;
            
            Mail::to($email, $nama)->send(new Tagihan($troli));

            return redirect('/transaksi/'.$troli->id)
                    ->with('sukses', 'Transaksi sukses, silahkan cek email anda dan lakukan pembayaran.');
        } else {

            return redirect()->route('troli')
                    ->with('gagal', 'Mohon cek kembali stok ketersediaan kami.');

        }

    }
}
