<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Troli;
use App\Mail\BuktiBayar;
use Mail;

class TransaksiController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

    public function transaksiku(Request $request)
    {
    	$troli = $request->user()->troli()->where('status_id', '>', '1')->get();
    	return view('transaksiku', [
    		'troliku' => $troli ,
    	]);
    }

    public function show(Request $request, Troli $transaksi)
    {
        $barangs = $transaksi->barangs()->get();
        return view('transaksi.show', [
            'trans' => $transaksi,
            'barangs' => $barangs,
        ]);
    }

    public function konfirm(Request $request, Troli $transaksi)
    {
        $request->validate([
            'nama' => 'required|string',
            'nominal' => 'required|numeric|min:'.$transaksi->harga() ,
            'bukti' => 'required|image|mimes:jpg,jpeg,png,bmp|max:2048',
        ]);

        $namaImage = 'bukti-'.$transaksi->id.'.'.$request->bukti->getClientOriginalExtension();
        $request->bukti->move(public_path('img/bukti'), $namaImage);

        $transaksi->bukti()->create([
            'nama' => $request->nama,
            'nominal' => $request->nominal,
            'bukti_image' => $namaImage, 
        ]);

        $transaksi->status_id = 3;
        $transaksi->save();

        Mail::to('rifqitaufiq4@gmail.com', 'Admin KasoCraft')->send(new BuktiBayar($transaksi));

        return redirect()->back()->with('sukses', 'bukti pembayaran telah diterima. Terima kasih telah belanja dan percaya padi kami, barang akan segera kami proses');
    }
}
