<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\Barang;
use App\Troli;
use DB;
use File;
use Auth;
use App\Mail\TransaksiOk;
use App\Mail\TransaksiGagal;
use Mail;

class AdminController extends Controller
{
    //
    //
    public function __construct()
    {
    	$this->middleware(['auth', 'admin']);
    }

    public function dashboard()
    {
        $transaksi = Troli::where('status_id', '>', '1')
                        ->orderBy('created_at','desc')->get();

    	return view('admin.dashboard', [
            'transaksi' => $transaksi,
    	]);
    }

    public function barang()
    {
        return view('admin.barang.index', [
            'kategori' => Kategori::all(),
            'barangs' => Barang::all(),
        ]);
    }

    public function storebarang(Request $request)
    {
    	$request->validate([
    		'nama' => 'required|string|unique:barangs' ,
    		'deskripsi' => 'required' ,
            'kategori' => 'required|numeric' ,
            'gambar' => 'image|mimes:jpg,jpeg,png,bmp|max:2048',
            'gram' => 'required|numeric|min:0',
    		'stok' => 'required|numeric|min:0' ,
    		'harga' => 'required|numeric|min:0' ,
    	]);

        $photoname = str_replace(' ','-', strtolower($request->nama)).'.'.$request->gambar->getClientOriginalExtension();
        $request->gambar->move(public_path('img/produk'), $photoname);

        DB::table('barangs')->insert([
            'nama' => $request->nama ,
            'deskripsi' => $request->deskripsi ,
            'kategori_id' => $request->kategori ,
            'stok' => $request->stok ,
            'harga' => $request->harga,
            'gram' => $request->gram,
            'nama_gambar' => $photoname,
        ]);


    	return redirect()->route('barang');
    }

    public function showEditBarang(Request $request, Barang $barang)
    {
    	$kategori = Kategori::all();
    	return view('admin.barang.editForm', [
    		'kategori' => $kategori,
    		'barang' => $barang,
    	]);
    }

    public function updatebarang(Request $request, Barang $barang)
    {
    	$request->validate([
    		'nama' => 'required|string' ,
    		'deskripsi' => 'required' ,
    		'kategori' => 'required|numeric' ,
            'gram' => 'required|numeric|min:0',
    		'stok' => 'required|numeric|min:0' ,
    		'harga' => 'required|numeric|min:0' ,
    	]);
        
        if ($request->hasFile('gambar')) {
            $request->validate([
                'gambar' => 'image|mimes:jpg,jpeg,png,bmp|max:2048',
            ]);

            File::delete('img/produk/'.$barang->nama_gambar);
            if (File::exists('img/produk/'.$barang->nama_gambar)) {
                return redirect('/admin/barang/edit/'.$barang->id);
            }
            $photoname = str_replace(' ','-', strtolower($request->nama)).'.'.$request->gambar->getClientOriginalExtension();
            $request->gambar->move(public_path('img/produk'), $photoname);

            $barang->update([
                'nama_gambar' => $photoname,
            ]);
        }
        $barang->update([
            'nama' => $request->nama,
            'deskripsi' => $request->deskripsi,
            'kategori_id' => $request->kategori,
            'gram' => $request->gram,
            'stok' => $request->stok,
            'harga' => $request->harga,
        ]);

    	return redirect()->route('barang');
    }

    public function showbarang(Request $request, Barang $barang)
    {
        return view('admin.barang.show', [
            'barang' => $barang,
        ]);
    }

    public function destroybarang(Request $request, Barang $barang)
    {
        File::delete('img/produk/'.$barang->nama_gambar);
        $barang->delete();

        return redirect()->route('barang');
    }

    public function showTransaksi(Request $request, Troli $transaksi)
    {
        $barangs = $transaksi->barangs()->get();
        return view('admin.transaksi.show', [
            'trans' => $transaksi,
            'barangs' => $barangs,
        ]);
    }

    public function eksekusiTransaksi(Request $request, Troli $transaksi)
    {
        $transaksi->keterangan = $request->keterangan;
        $transaksi->status_id = $request->submit;

        $transaksi->save();

        if ($request->submit == 5)
        {
            $barangs = $transaksi->barangs()->get();
            foreach ($barangs as $barang) {
                $barang->stok += $barang->pivot->qty;
                $barang->save();
            }
        }

        $mail = Mail::to($transaksi->user->email, $transaksi->user->nama);
        if ($transaksi->status_id == 4) {
            $mail->send(new TransaksiOk($transaksi));
        } else {
            $mail->send(new TransaksiGagal($transaksi));
        }
        return redirect()->route('transaksi');
    }
}
