<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\Kategori;
class ProdukController extends Controller
{

    private function viewProduk($barangs)
    {
        return view('produk', [
            'barangs' => $barangs,
            'listKategori' => Kategori::all(),
        ]);
    }
    public function index()
    {
   	    $barangs = Barang::paginate(12);
    	return $this->viewProduk($barangs);
    }

    public function show(Request $request, Barang $barang)
    {
    	return view('produk.detail', [
    		'barang' => $barang
    	]);	
    }

    public function kategori(Request $request, Kategori $kategori)
    {
        $barangs = $kategori->barangs()->paginate(12);
        return $this->viewProduk($barangs);
    }

    public function search(Request $request)
    {
        $pencarian = $request->query('cari');

        if (!empty($pencarian)) {
            $barangs = Barang::where('nama','like', '%'.$pencarian.'%')
                                ->orWhere('deskripsi', 'like', '%'.$pencarian.'%')
                                ->orWhere('nama_gambar', 'like', '%'.$pencarian.'%')
                                ->paginate(12);
        } else {
            $barangs = Barang::paginate(12);
        }

        return $this->viewProduk($barangs);
    }

}
