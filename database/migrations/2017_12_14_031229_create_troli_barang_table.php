<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTroliBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('troli_barang', function (Blueprint $table) {
            $table->integer('troli_id')->unsigned()->index();
            $table->integer('barang_id')->unsigned()->index();
            $table->integer('qty')->unsigned();
            $table->primary(['troli_id', 'barang_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('troli_barang');
    }
}
