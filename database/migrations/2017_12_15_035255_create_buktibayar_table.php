<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuktibayarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buktibayar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('troli_id')->unsigned()->index();
            $table->string('nama');
            $table->integer('nominal')->unsigned();
            $table->string('bukti_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buktibayar');
    }
}
