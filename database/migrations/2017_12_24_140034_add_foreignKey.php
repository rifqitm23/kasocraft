<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('barangs', function (Blueprint $table) {
            $table->foreign('kategori_id')->references('id')->on('kategori');
        });
        Schema::table('troli', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('status_id')->references('id')->on('status');
        });
        Schema::table('troli_barang', function (Blueprint $table) {
            $table->foreign('troli_id')->references('id')->on('troli');
            $table->foreign('barang_id')->references('id')->on('barangs');
        });
        Schema::table('buktibayar', function (Blueprint $table) {
            $table->foreign('troli_id')->references('id')->on('troli');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barangs', function (Blueprint $table) {
            $table->dropForeign('kategori_id');
        });
        Schema::table('troli', function (Blueprint $table) {
            $table->dropForeign('user_id');
            $table->dropForeign('status_id');
        });
        Schema::table('troli_barang', function (Blueprint $table) {
            $table->dropForeign('troli_id');
            $table->dropForeign('barang_id');
        });
        Schema::table('buktibayar', function (Blueprint $table) {
            $table->dropForeign('troli_id');
        });
    }
}
