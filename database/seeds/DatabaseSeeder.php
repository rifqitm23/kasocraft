<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        (DB::table('kategori')->count() > 0) or $this->kategoriSeeder();
        
        (DB::table('status')->count() > 0) or $this->statusSeeder();
        
        (DB::table('users')->count() > 0) or $this->adminSeeder();
        
        (DB::table('barangs')->count() > 0) or $this->barangSeeder();
    }

    public function statusSeeder()
    {
        $statuses = [
            'aktif', 
            'belum dibayar', 
            'sudah dibayar',
            'ok',
            'gagal',
        ];

        foreach($statuses as $status) {
            DB::table('status')->insert([
                'status' => $status
            ]);
        }
    }

    public function adminSeeder()
    {
        DB::table('users')->insert([
                'nama' => 'admin',
                'email' => 'admin@kaso.id',
                'password' => bcrypt('admin123'),
                'alamat' => 'FTI UII Jl. Kaliurang km 13.5 Besi Sleman Yogyakarta',
                'no_telp' => '0821638XXX',
                'jabatan' => '2',
                'confirm' => '2'
        ]);
        
        DB::table('users')->insert([
            'nama' => 'user' ,
            'email' => 'rifqitaufiq60@gmail.com' ,
            'password' => bcrypt('user123') ,
            'alamat' => 'Jojga' ,
            'no_telp' => '0987652153' ,
            'jabatan' => '1' ,
            'confirm' => '1'
        ]);
    }

    public function barangSeeder()
    {
        $deskripsi = [
            'barang terbaik yang pernah ada',
            'barang murah',
            'barang bagus jangan dilewatkan',
            'barang unik + langka'
        ];

        for ($i = 0; $i < 30; $i++){ // create 5 barang random without picture
            DB::table('barangs')->insert([
                'nama' => 'barang '.($i + 1),
                'deskripsi' => $deskripsi[rand(0,3)],
                'kategori_id' => rand(1,3),
                'gram' => rand(5, 20) * 100,
                'stok' => rand(10, 100),
                'harga' => rand(5, 15) * 1000,
                'nama_gambar' => 'default'.rand(1,10).'.jpg',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }

    public function kategoriSeeder()
    {
        DB::table('kategori')->insert([
            'nama' => 'Gerabah',
            'slug' => 'gerabah',
        ]);
        DB::table('kategori')->insert([
            'nama' => 'Souvenir',
            'slug' => 'souvenir',
        ]);
        DB::table('kategori')->insert([
            'nama' => 'Tanah Liat',
            'slug' => 'tanah-liat',
        ]);
    }
}
