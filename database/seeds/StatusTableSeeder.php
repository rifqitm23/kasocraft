<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$statuses = [
    		'aktif', 
    		'belum dibayar', 
    		'sudah dibayar',
    		'ok',
    		'gagal',
    	];

        foreach($statuses as $status) {
	        DB::table('status')->insert([
	        	'status' => $status
	        ]);
        }
    }
}
