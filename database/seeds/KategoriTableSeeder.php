<?php

use Illuminate\Database\Seeder;

class KategoriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kategori')->insert([
        	'nama' => 'Gerabah',
            'slug' => 'gerabah',
        ]);
        DB::table('kategori')->insert([
        	'nama' => 'Souvenir',
            'slug' => 'souvenir',
        ]);
        DB::table('kategori')->insert([
        	'nama' => 'Tanah Liat',
            'slug' => 'tanah-liat',
        ]);
    }
}
