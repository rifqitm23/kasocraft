<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for ($i = 0; $i < 15; $i++){ // create 5 barang random without picture
	        DB::table('barangs')->insert([
	    		'nama' => str_random(10),
	    		'deskripsi' => str_random(50),
	    		'kategori_id' => rand(1,3),
	    		'gram' => '700',
	    		'stok' => '20',
	    		'harga' => '5000',
	    		'nama_gambar' => 'vas-bunga.jpg',
	    		'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
	    		'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
	        ]);
    	}

    }
}
