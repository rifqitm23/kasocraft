<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            	'nama' => 'admin',
                'email' => 'admin@kaso.id',
            	'password' => bcrypt('admin123'),
            	'alamat' => 'FTI UII Jl. Kaliurang km 13.5 Besi Sleman Yogyakarta',
            	'no_telp' => '0821638XXX',
                'jabatan' => '2',
                'confirm' => '2'
            ]);
    }
}
