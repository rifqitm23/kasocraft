@extends('layouts.app')

@section('content')
	
	<div class="container">
		<div class="row" style="margin:auto 0px;">
			<div class="panel panel-default">
				<div class="panel-body">
					<img src="{{ asset('img/troliku.png') }}" width="27%"><br>
					{{ csrf_field() }}
					{{ method_field('PUT') }}
					<table class="table">
						<thead>
							<tr>
								<th></th>
								<th>Produk</th>
								<th>Berat</th>
								<th>Harga</th>
								<th>Qty</th>
								<th>Total Berat</th>
								<th>Total Harga</th>
							</tr>
						</thead>
						<!-- <form action="" method="POST"> -->
							<tbody>
								@foreach ($barang_orderan as $item)
								<tr>
									<td align="right">
										<form action="{{ url('/troli/asdfgh/'.$idTroli.'/'.$item->id) }}" method="POST"
											onsubmit="return confirm('anda yakin untuk tidak membelinya?');">
											{{ csrf_field() }}
											{{ method_field('DELETE') }}
											<button type="submit" class="btn btn-danger">
												Hapus
											</button>
										</form>
									</td>
									<td>{{ $item->nama }}</td>
									<td>{{ $item->beratkg() }}</td>
									<td>{{ $item->hargarupiah() }}</td>
									<td>
										<form action="{{ url('/troli/asdfgh/'.$idTroli.'/'.$item->id) }}" method="POST">
											{{ csrf_field() }}
											{{ method_field('PUT') }}

											<input class="form-control" style="width:20%;display:inline;" 
												name="qty" value="{{ $item->pivot->qty }}"> 
											<button type="submit" class="btn btn-warning">Ubah</button><b> Tersedia : {{ $item->stok }}</b>
										</form>
									</td>
									<td>{{ (float) $item->beratkg() * $item->pivot->qty }} kg</td>
									<td>{{ "Rp ".number_format($item->harga * $item->pivot->qty,2,',','.') }}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr class="active">
									<td colspan="5"></td>
									<td style="vertical-align:middle;"><strong>Total Keseluruhan</strong></td>
									<td style="font-size:2em"><strong>{{ $total }}</strong></td>
								</tr>
							</tfoot>
						<!-- </form> -->
					</table>
					<div class="text-right">
						<form action="{{ url('/troli/bayar/'.$idTroli) }}" method="POST"
								onsubmit="return confirm('anda yakin tidak ada barang yang akan dibeli lagi ?');">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-lg btn-primary">
								Bayar
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection