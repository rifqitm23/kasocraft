@extends('admin.template')

@section('content')
<div class="container-fluid">
<!-- FORM TAMBAH BARANG -->
	<div class="panel panel-default">
		<div class="panel-heading">
			Tambah Barang
		</div>
		<div class="panel-body">
			<div class="col-sm-12">
				<form class="form-horizontal" method="POST" action="{{ url('/admin/barang/tambah') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group{{ $errors->has('nama') ? ' has-error' : ''}}">
						<label class="control-label col-sm-2" for="nama-barang">Nama:</label>
						<div class="col-sm-9">
							<input type="text" name="nama" class="form-control" id="nama-barang" placeholder="Input nama barang" value="{{ old('nama') }}">
							@if ($errors->has('nama'))
								<span class="help-block">
									<strong>{{ $errors->first('nama') }}</strong>									
								</span>
							@endif
						</div>
					</div>
					<div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : ''}}">
						<label class="control-label col-sm-2" for="deskripsi-barang">Deskripsi:</label>
						<div class="col-sm-9"> 
							<textarea class="form-control" name="deskripsi" id="deskripsi-barang" name="deskripsi" rows="3">{{ old('deskripsi') }}</textarea>
							@if ($errors->has('deskripsi'))
								<span class="help-block">
									<strong>{{ $errors->first('deskripsi') }}</strong>									
								</span>
							@endif
						</div>
					</div>
					<div class="form-group{{ $errors->has('kategori') ? ' has-error' : ''}}">
						<label class="control-label col-sm-2" for="kategori-barang">Kategori:</label>
						<div class="col-sm-9"> 
							<select id="kategori-barang" name="kategori" class="form-control">
								@foreach ($kategori as $item)
									<option value="{{$item->id}}" {{ $item->id == old('kategori') ? 'selected' : '' }}>{{$item->nama}}</option>
								@endforeach
							</select>
							@if ($errors->has('kategori'))
								<span class="help-block">
									<strong>{{ $errors->first('kategori') }}</strong>
								</span>
							@endif
						</div>
					</div>
					<div class="form-group{{ $errors->has('gambar') ? ' has-error' : ''}}">
						<label class="control-label col-sm-2" for="gambar-barang">Gambar:</label>
						<div class="col-sm-9"> 
							<input type="file" name="gambar" class="form-control" id="gambar-barang" value="{{ old('gambar') }}">
							@if ($errors->has('gambar'))
								<span class="help-block">
									<strong>{{ $errors->first('gambar') }}</strong>									
								</span>
							@endif
						</div>
					</div>
					<div class="form-group{{ $errors->has('gram') ? ' has-error' : ''}}">
						<label class="control-label col-sm-2" for="berat-barang">Berat(gram):</label>
						<div class="col-sm-9"> 
							<input type="number" name="gram" class="form-control" id="berat-barang" value="{{ old('gram') }}">
							@if ($errors->has('gram'))
								<span class="help-block">
									<strong>{{ $errors->first('gram') }}</strong>									
								</span>
							@endif
						</div>
					</div>
					<div class="form-group{{ $errors->has('stok') ? ' has-error' : ''}}">
						<label class="control-label col-sm-2" for="stok-barang">Stok:</label>
						<div class="col-sm-9"> 
							<input type="number" name="stok" class="form-control" id="stok-barang" value="{{ old('stok') }}">
							@if ($errors->has('stok'))
								<span class="help-block">
									<strong>{{ $errors->first('stok') }}</strong>									
								</span>
							@endif
						</div>
					</div>
					<div class="form-group{{ $errors->has('harga') ? ' has-error' : ''}}">
						<label class="control-label col-sm-2" for="harga-barang">Harga:</label>
						<div class="col-sm-9"> 
							<input type="number" name="harga" class="form-control" id="harga-barang" value="{{ old('harga') }}">
							@if ($errors->has('harga'))
								<span class="help-block">
									<strong>{{ $errors->first('harga') }}</strong>									
								</span>
							@endif
						</div>
					</div>
					<div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-9">
							<button type="submit" class="btn btn-default">Tambah</button>
						</div>
					</div>
				</form>
			</div>
			<div class="col-sm-12">
				<!-- <h1>Info lainnya</h1> -->
				<input class="form-control" id="searchTableBarang" type="text" placeholder="Search..">
			<br>
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th style="width:5%">id</th>
						<th style="width:20%">nama</th>
						<th style="width:10%">kategori</th>
						<th style="width:10%">berat</th>
						<th style="width:10%">stok</th>
						<th style="width:10%">harga</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="tableBarang">
					@foreach ($barangs as $barang)
						<tr>
							<td>{{ $barang->id }}</td>
							<td>{{ $barang->nama }}</td>
							<td>{{ $barang->kategori->nama }}</td>
							<td>{{ $barang->gram }} gram</td>
							<td>{{ $barang->stok }}</td>
							<td>{{ $barang->hargarupiah() }}</td>
							<td>
								<a type="button" class="btn btn-info" href="{{ url('/admin/barang/'.$barang->id) }}">Show</a>
								<a type="button" class="btn btn-warning" href="{{ url('/admin/barang/edit/'.$barang->id) }}">Edit</a>
								<form style="display:inline;" action="{{ url('admin/barang/delete/'.$barang->id) }}" method="POST" 
									onsubmit="return confirm('Anda yakin menghapus barang {{ $barang->nama }} dari database?');">
									{{ csrf_field() }}
									{{ method_field('DELETE') }}

									<button type="submit" class="btn btn-danger">Delete</button>
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script>
	$(document).ready(function(){
	  $("#searchTableBarang").on("keyup", function() {
	    var value = $(this).val().toLowerCase();
	    $("#tableBarang tr").filter(function() {
	      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
	    });
	  });
	});
</script>
@endsection