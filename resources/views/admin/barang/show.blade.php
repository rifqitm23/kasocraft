@extends('admin.template')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-6">
						<table class="table">
							<thead>
								<th>Informasi Produk</th>
								<th><a style="padding-right:30px; padding-left:30px;" type="button" class="btn btn-sm btn-primary" href="{{ url('admin/barang/edit/'.$barang->id) }}">Edit</a></th>
							</thead>
							<tbody>
								<tr><td>Harga Satuan</td><td>{{ $barang->hargarupiah() }}</td></tr>
								<tr><td>Berat</td><td>{{ $barang->beratkg() }}</td></tr>
								<tr><td>Stok</td><td>{{ $barang->stok }}</td></tr>
								<tr><td>Deskripsi</td><td>{{ $barang->deskripsi }}</td></tr>
							</tbody>
						</table>
					</div>
					<div class="col-sm-6 text-right">
						<img src="{{ $barang->gambar() }}" width="50%">
						<p>{{ $barang->nama_gambar }}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection