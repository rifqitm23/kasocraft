@extends('admin.template')
@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		Edit Barang
	</div>
	<div class="panel-body">
		<div class="col-sm-6">
			<form class="form-horizontal" method="POST" action="{{ url('/admin/barang/edit/'.$barang->id) }}" enctype="multipart/form-data">
				{{ method_field('PUT') }}
				{{ csrf_field() }}
				<div class="form-group{{$errors->has('nama') ? ' has-error' : ''}}">
					<label class="control-label col-sm-2" for="nama-barang">Nama:</label>
					<div class="col-sm-10">
						<input type="text" name="nama" class="form-control" id="nama-barang" placeholder="Input nama barang" value="{{ $barang->nama }}">
						@if ($errors->has('nama'))
							<span class="help-block">
								<strong>{{ $errors->first('nama') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : ''}}">
					<label class="control-label col-sm-2" for="deskripsi-barang">Deskripsi:</label>
					<div class="col-sm-10"> 
						<textarea class="form-control" name="deskripsi" id="deskripsi-barang" name="deskripsi" rows="3">{{ $barang->deskripsi }}</textarea>
						@if ($errors->has('deskripsi'))
							<span class="help-block">
								<strong>{{ $errors->first('deskripsi') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="form-group{{ $errors->has('kategori') ? ' has-error' : ''}}">
					<label class="control-label col-sm-2" for="kategori-barang">Kategori:</label>
					<div class="col-sm-10"> 
						<select id="kategori-barang" name="kategori" class="form-control">
							@foreach ($kategori as $item)
								<option value="{{$item->id}}" {{ $item->id == $barang->kategori->id ? 'selected' : ''}}>{{$item->nama}}</option>
							@endforeach
						</select>
						@if ($errors->has('kategori'))
							<span class="help-block">
								<strong>{{ $errors->first('kategori') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="form-group{{ $errors->has('gambar') ? ' has-error' : ''}}">
					<label class="control-label col-sm-2" for="gambar-barang">Gambar:</label>
					<div class="col-sm-10"> 
						<input type="file" name="gambar" class="form-control" id="gambar-barang">
						@if ($errors->has('gambar'))
							<span class="help-block">
								<strong>{{ $errors->first('gambar') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="form-group{{ $errors->has('gram') ? ' has-error' : ''}}">
					<label class="control-label col-sm-2" for="berat-barang">Berat(gram):</label>
					<div class="col-sm-10"> 
						<input type="number" id="berat-barang" name="gram" class="form-control" value="{{ $barang->gram }}">
						@if ($errors->has('gram'))
							<span class="help-block">
								<strong>{{ $errors->first('gram') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="form-group{{ $errors->has('stok') ? ' has-error' : ''}}">
					<label class="control-label col-sm-2" for="stok-barang">Stok:</label>
					<div class="col-sm-10"> 
						<input type="number" name="stok" class="form-control" id="stok-barang" value="{{ $barang->stok }}">
						@if ($errors->has('stok'))
							<span class="help-block">
								<strong>{{ $errors->first('stok') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="form-group{{ $errors->has('harga') ? ' has-error' : ''}}">
					<label class="control-label col-sm-2" for="harga-barang">Harga:</label>
					<div class="col-sm-10"> 
						<input type="number" name="harga" class="form-control" id="harga-barang" value="{{ $barang->harga }}">
						@if ($errors->has('harga'))
							<span class="help-block">
								<strong>{{ $errors->first('harga') }}</strong>
							</span>
						@endif					
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default">Ubah</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-sm-6 text-right">
			<img id="preview" src="{{ asset('img/produk/'.$barang->nama_gambar) }}" width="50%">
		</div>
	</div>
</div>
@endsection

@section('js')
<script>
	function readUrl(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			var ValidImagesTypes = ['image/gif', 'image/jpeg', 'image/png'];
			if ($.inArray(input.files[0]['type'], ValidImagesTypes) < 0) {
				alert('File harus berbentuk gambar jpeg/jpg/png/bmp');
			} else {
				reader.onload = function (e) {
					var image = new Image();
					image.src = e.target.result;
					image.onload = function () {
						$('#preview').attr('src', e.target.result);
					};
				};
				reader.readAsDataURL(input.files[0]);			
			}
		}
	}
	$('#gambar-barang').change(function(){
		readUrl(this);
	});
</script>
@endsection