@extends('admin.template')

@section('content')
<div class="container-fluid">
		<!-- Transaksi -->
	<div class="panel panel-default">
		<div class="panel-body">
			<input class="form-control" id="searchTableBarang" type="text" placeholder="Search..">
			<br>
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th style="width:5%">id Transaksi</th>
						<th style="width:20%">Tanggal</th>
						<th style="width:20%">Pembeli</th>
						<th style="width:10%">Total</th>
						<th style="width:10%">Status pembayaran</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="tableBarang">
					@foreach($transaksi as $trans)
						<tr>
							<td>{{ $trans->id }}</td>
							<td>{{ $trans->tgl_beli }}</td>
							<td>{{ $trans->user->nama }}</td>
							<td>{{ $trans->totalHarga() }}</td>
							<td class="success">{{ $trans->status->status }}</td>
							<td>
								<a href="{{ url('/admin/transaksi/'.$trans->id) }}" type="button" class="btn btn-primary">Lihat Transaksi</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			
		</div>
	</div>

</div>
@endsection

@section('js')
<script>
	$(document).ready(function(){
	  $("#searchTableBarang").on("keyup", function() {
	    var value = $(this).val().toLowerCase();
	    $("#tableBarang tr").filter(function() {
	      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
	    });
	  });
	});
</script>
@endsection