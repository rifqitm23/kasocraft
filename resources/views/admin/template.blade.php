<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<style>

</style>
@yield('css')
<body>

    <div id="app">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="{{ route('home') }}" class="navbar-brand"><img height="100%" src="{{ asset('img/header-kasocraft.png') }}"></a>
                </div>
                <ul class="nav navbar-nav">
                    <li{{ Route::currentRouteName() == 'transaksi' ? ' class=active' : ''}}><a href="{{ route('transaksi') }}">Transaksi</a></li>
                    <li{{ Route::currentRouteName() == 'barang' ? ' class=active' : ''}}><a href="{{ route('barang') }}">Barang</a></li>
<!--                     <li><a href="#">Transaksi</a></li>
                    <li><a href="#">Pengaturan</a></li> -->
                </ul>
            </div>
        </nav>

        @yield('content')
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('js')


</body>
</html>
