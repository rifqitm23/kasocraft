@extends('admin.template')
@section('content')
<div class="container">
<div class="panel panel-default">
	<div class="panel-body">
		
	<!-- INFORMASI TRANSAKSI -->
	<div class="col-md-3">
	<h1>Transaksi {{ $trans->id}}</h1><br>
		<table class="table" style="">
			<tr><td><b>Id Transaksi</b></td><td> {{ $trans->id }}</td></tr>
			<tr><td><b>Pembeli</b></td><td> {{ $trans->user->nama }}</td></tr>
			<tr><td><b>Alamat</b></td><td> {{ $trans->user->alamat }}</td></tr>
			<tr><td><b>Status</b></td><td class="danger"> {{ $trans->status->status }}</td></tr>
		</table>
	</div>
	<div class="col-md-1"></div>
	<div class="col-md-3 text-center">
	@if (count($trans->bukti) > 0)
		<a href="#" data-toggle="modal" data-target="#preview">
			<img src="{{ asset('img/bukti/'.$trans->bukti->bukti_image) }}" width="100%"><br>
		</a>	
		<p>Klik perbesar</p>
	@endif
	</div>
	<div class="col-md-5">
	@if ($trans->status_id < 4)
		<h2>Proses transaksi</h2>
		<form class="form-horizontal" action="{{ url('/admin/transaksi/'.$trans->id) }}" method="POST">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<label for="keterangan">Keterangan</label>
			<textarea id="keterangan" name="keterangan" rows="4" class="form-control"></textarea>
			<div style="padding:20px" class="text-center">
	@if ($trans->status_id == 3)
				<button type="submit" class="btn btn-primary" name="submit" value="4">Sukses</button> Atau
	@endif
				<button type="submit" class="btn btn-danger" name="submit" value="5">Gagal</button>
			</div>
		</form>
		@endif
	</div>

	<div class="col-md-12">
	<!-- BARANG TRANSAKSI -->
		<table class="table table-hover table-bordered">
			<thead>
				<tr class="active">
					<th>Produk</th>
					<th>Berat</th>
					<th>Harga</th>
					<th>Qty</th>
					<th>Total Berat</th>
					<th>Total Harga</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($barangs as $barang)
					<tr>
						<td>{{ $barang->nama }}</td>
						<td>{{ $barang->beratkg() }}</td>
						<td>{{ $barang->hargarupiah() }}</td>
						<td>{{ $barang->pivot->qty }}</td>
						<td>{{ (float) $barang->gram / 1000 * $barang->pivot->qty }} kg</td>
						<td>{{ "Rp ".number_format($barang->harga * $barang->pivot->qty,2,',','.') }}</td>
					</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr class="active">
					<td colspan="4"></td>
					<td style="vertical-align:middle;"><strong>Total Keseluruhan</strong></td>
					<td style="font-size:2em"><strong>{{ $trans->totalHarga() }}</strong></td>
				</tr>
			</tfoot>
		</table>
	</div>
	</div>
</div>
</div>
@if (count($trans->bukti) > 0)
<div id="preview" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<img src="{{ asset('img/bukti/'.$trans->bukti->bukti_image) }}" width="100%">
			</div>
		</div>
	</div>
</div>
@endif
@endsection