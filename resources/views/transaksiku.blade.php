@extends('layouts.app')
@section('content')
<div class="container">
	<h1>Transaksiku</h1><br>
	<table class="table table-hover">
		<thead>
			<tr>
				<th>id Transaksi</th>
				<th>Tanggal</th>
				<th>Total Harga</th>
				<th>Action</th>
				<th>status</th>
				<th>keterangan</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($troliku as $troli)
				<tr>
					<td>{{ $troli->id }}</td>
					<td>{{ $troli->updated_at }}</td>
					<td>{{ $troli->totalHarga() }}</td>
					<td>
						<a type="button" class="btn btn-primary" href="{{ url('/transaksi/'.$troli->id) }}">Lihat</a>
					</td><td>
						@if ($troli->status_id === 2)
							<a type="button" href="{{ url('/transaksi/'.$troli->id) }}" class="btn btn-warning">Konfirmasi Pembayaran</a>
						@elseif ($troli->status_id === 3)
							<span class="label label-warning">Menunggu Admin</span>
						@elseif ($troli->status_id === 4)
							<span class="label label-success">Selesai</span>
						@elseif ($troli->status_id === 5)
							<span class="label label-danger">Transaksi gagal</span>
						@endif
					</td>
					<td>{{ $troli->keterangan }}</td>
				</tr>
			@endforeach

		</tbody>
	</table>
</div>
@endsection