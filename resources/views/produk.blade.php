@extends('layouts.app')

@section('content')

<div class="container">

	<div class="row text-center" style="margin-bottom:30px;">
		<div class="form-group">
			
			<div class="btn-group btn-group-justified">
  				@foreach ($listKategori as $item)
  				<a href="{{ url('/produk/kategori/'.$item->slug) }}" class="btn btn-default btn-black">{{ $item->nama }}</a>
  				@endforeach
				
			</div>
		</div>
	</div>
	<div class="row text-center">
		@foreach($barangs as $barang)
		<div class="col-xs-6 col-sm-4 col-md-3" style="padding:0px 10px;">
			<div class="card">
				<a href="{{ url('produk/detail/'.$barang->id) }}"><img src="{{ asset('img/produk/'.$barang->nama_gambar) }}" width="100%"></a>
				<div class="card-title">
					<h2>{{ $barang->nama }}</h2>
					<span style="color:red;">{{ $barang->hargarupiah() }}</span>
					<a href="{{ url('produk/detail/'.$barang->id) }}" class="card-btn">Lihat</a>
				</div>
			</div>
		</div>
		@endforeach
		{{ $barangs->links() }}
	</div>
</div>


@endsection
@section('css')
<style>
	.btn-black {
		background:black;
		border-color:#fff;
		color:#fff;	
	}
	.btn-black:hover, .btn-black:active, .btn-black:focus, .btn-black:active:hover {
		background:#444;
		color:#fff;	
	}
</style>
@endsection