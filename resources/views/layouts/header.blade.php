<header>
	<div class="text-center">
		<img src="{{ asset('img/header-kasocraft.png') }}" width="50%" />
	</div>
	<ul style="background:#fff;" class="nav nav-tabs nav-justified">
		<li{{ Route::currentRouteName() == 'home' ? ' class=active' : ''}}>
			<a href="{{route('home')}}">Beranda</a>
		</li>
		<li{{ Request::is('produk') || Request::is('produk/*') ? ' class=active' : ''}}>
			<a href="{{ route('produk') }}">Produk</a>
		</li>
		<li{{ Route::currentRouteName() == 'carapesan' ? ' class=active' : ''}}>
			<a href="{{ route('carapesan') }}">Cara Pesan</a>
		</li>
		<li {{ (Route::currentRouteName() == 'tentang') ? ' class=active' : '' }}>
			<a href="{{ route('tentang') }}">Tentang</a>
		</li>
	</ul>
</header>