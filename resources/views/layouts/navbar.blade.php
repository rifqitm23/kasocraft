<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <!-- Authentication Links -->
                @guest
                    <li>
                        <a data-toggle="modal" data-target="#login" href="#">Login</a>
                    </li>
                    <li>
                        <a data-toggle="modal" data-target="#daftar" href="#">Daftar</a>
                    </li>
                    <li>
                      <a href="{{ route('troli') }}">
                          <img src="{{  asset('img/troli.png') }}" width="20" /> &nbsp;Troli
                      </a>
                    </li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::user()->nama }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                <!-- Chart Link -->
                  @if (Auth::user()->jabatan == 2)
                    <li><a href="{{ route('admin') }}">Dashboard Admin</a></li>
                  @else
                    <li><a href="{{ route('transaksiku') }}">Transaksiku</a></li>
                    <li>
                      <a href="{{ route('troli') }}">
                          <img src="{{  asset('img/troli.png') }}" width="20" /> &nbsp;Troli
                      </a>
                    </li>
                  @endif
                @endguest
            </ul>

            <!-- Right Side Of Navbar -->
            <div class="nav navbar-nav navbar-right">
                <form class="navbar-form" role="search" style="float:right; margin:0px; padding:5px 15px" action="{{ url('/q')}}" method="GET">
                    <div class="input-group">
                      <input type="text" class="nav-search form-control" style="font-weight:normal;" name="cari" id="srch-term" placeholder="Cari barang...">
                      <div class="input-group-btn">
							          <button class="btn btn-success" type="submit">Cari</button>
                      </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</nav>

@guest
<div class="modal fade" id="login" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Login</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <!-- Modal body -->
    <div class="modal-body">
      <div>
        <form action="{{ route('login') }}" method="POST">
            {{ csrf_field() }}
          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="login-email">Email :</label>
              <input type="email" class="form-control" name="email" id="login-email" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
              <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label for="login-password">Password :</label>
              <input type="password" class="form-control" name="password" id="login-password" required>
            @if ($errors->has('password'))
              <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
            @endif
          </div>
          <div class="checkbox">
             <label><input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>  Ingat saya</label>
          </div>
          <button type="submit" class="btn btn-default">Login</button>
        </form>
      </div>
    </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <img src="{{ asset('img/header-kasocraft.png') }}" width="20%" align="center">
      </div>

    </div>
  </div>
</div>

<!-- MODAL DAFTAR -->
<div class="modal fade" id="daftar" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Daftar</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <!-- Modal body -->
    <div class="modal-body">
      <div>
        <form action="{{ route('register') }}" method="POST">
            {{ csrf_field() }}
          <div class="form-group{{ $errors->daftar->has('nama') ? ' has-error' : '' }}">
              <label for="nama">Nama :</label>
              <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama') }}" required>
            @if ($errors->daftar->has('nama'))
              <span class="help-block">
                  <strong>{{ $errors->daftar->first('nama') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group{{ $errors->daftar->has('no_telp') ? ' has-error' : '' }}">
              <label for="no_telp">No. Handphone :</label>
              <input type="text" class="form-control" name="no_telp" id="no_telp" value="{{ old('no_telp') }}" required>
            @if ($errors->daftar->has('no_telp'))
              <span class="help-block">
                  <strong>{{ $errors->daftar->first('no_telp') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group{{ $errors->daftar->has('alamat') ? ' has-error' : '' }}">
              <label for="alamat">Alamat</label>
              <input type="text" class="form-control" name="alamat" id="alamat" value="{{ old('alamat') }}" required>
            @if ($errors->daftar->has('alamat'))
              <span class="help-block">
                  <strong>{{ $errors->daftar->first('alamat') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group{{ $errors->daftar->has('email') ? ' has-error' : '' }}">
              <label for="email">Email :</label>
              <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}" required>
            @if ($errors->daftar->has('email'))
              <span class="help-block">
                  <strong>{{ $errors->daftar->first('email') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group{{ $errors->daftar->has('password') ? ' has-error' : '' }}">
              <label for="password">Password :</label>
              <input type="password" class="form-control" name="password" id="password" required>
            @if ($errors->daftar->has('password'))
              <span class="help-block">
                  <strong>{{ $errors->daftar->first('password') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group">
              <label for="password-confirm">Confirm Password</label>
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
          </div>

          <div class="form-group">
            <button type="submit" class="btn btn-default">Daftar</button>
          </div>
        </form>
      </div>
    </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <img src="{{ asset('img/header-kasocraft.png') }}" width="20%" align="center">
      </div>

    </div>
  </div>
</div>

@endguest