<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/produk-card.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet">
</head>
<style>
    body {
        background-color:#fff;
    }
    nav {
        font-weight:700;
    }
    .tab-menu {
        float:left;
    }
    header {
        margin-bottom:25px;
    }
    footer {
        text-align:center;
        background:#000000;
        color:#fff;
        padding:10px 0px;
        margin-top:25px;
    }
</style>
@yield('css')
<body>
    <div id="app">
        @include('layouts.navbar')
        @include('layouts.header')
        
        @yield('content')

        <footer>
            <p>Copyright &copy; kasocraft 2017.</p>
        </footer>
    </div>

    <!-- Alert Notification -->
    @if (session('sukses'))
        <script>alert("{{ session('sukses') }}")</script>
    @elseif (session('gagal'))
        <script>alert("{{ session('gagal') }}")</script>
    @endif
    
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
    @yield('js')

    @guest
        @if(count($errors->daftar) > 0)
            <script>
                $("#daftar").modal("show");
            </script>
        @elseif(count($errors) > 0 || session('mustLogin'))
            <script>
                $("#login").modal("show");
            </script>
        @endif
    @endguest

</body>
</html>
