@extends('layouts.app')
@section('content')
<div class="container">

	<!-- INFORMASI TRANSAKSI -->
	<div class="col-md-4">
	<h1>Transaksiku</h1><br>
		<table class="table">
			<tr><td><b>Id Transaksi</b></td><td> {{ $trans->id }}</td></tr>
			<tr><td><b>Pembeli</b></td><td> {{ $trans->user->nama }}</td></tr>
			<tr><td><b>Alamat tujuan</b></td><td> {{ $trans->user->alamat }}</td></tr>
			<tr><td><b>Status</b></td><td class="danger"> {{ $trans->status->status }}</td></tr>
		</table>
	</div>
	@if ($trans->status_id == 2)
		<div class="col-md-3"></div>
		<div class="col-md-5">

			<h1 class="danger">Konfirmasi Pembayaran</h1><br>
			<!-- FORM KONFIRMASI -->
			<form class="form-horizontal" action="{{ url('/transaksi/konfirmasi/'.$trans->id) }}" enctype="multipart/form-data" method="POST">
				{{ csrf_field() }}
				<div class="form-group">
					<div class="col-sm-4">
						<label for="nama" class="label-control">Atas Nama</label>
					</div>
					<div class="col-sm-8">
						<input type="text" class="col-sm-8 form-control" name="nama" id="nama">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<label for="nominal" class="label-control">Nominal uang</label>
					</div>
					<div class="col-sm-8">
						<input type="number" class="col-sm-8 form-control" name="nominal" id="nominal">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<label for="bukti" class="label-control">Bukti Pembayaran</label>
					</div>
					<div class="col-sm-8">
						<input type="file" class="col-sm-8 form-control" name="bukti" id="bukti">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
					</div>
					<div class="col-sm-8">
						<button type="submit" class="btn btn-primary">Konfirmasi</button>
					</div>
				</div>

			</form>
		</div>
	@endif
	<!-- BARANG TRANSAKSI -->
	<table class="table table-hover table-bordered">
		<thead>
			<tr class="active">
				<th>Produk</th>
				<th>Berat</th>
				<th>Harga</th>
				<th>Qty</th>
				<th>Total Berat</th>
				<th>Total Harga</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($barangs as $barang)
				<tr>
					<td>{{ $barang->nama }}</td>
					<td>{{ $barang->beratkg() }}</td>
					<td>{{ $barang->hargarupiah() }}</td>
					<td>{{ $barang->pivot->qty }}</td>
					<td>{{ (float) $barang->gram / 1000 * $barang->pivot->qty }} kg</td>
					<td>{{ "Rp ".number_format($barang->harga * $barang->pivot->qty,2,',','.') }}</td>
				</tr>
			@endforeach
		</tbody>
		<tfoot>
			<tr class="active">
				<td colspan="4"></td>
				<td style="vertical-align:middle;"><strong>Total Keseluruhan</strong></td>
				<td style="font-size:2em"><strong>{{ $trans->totalHarga() }}</strong></td>
			</tr>
		</tfoot>
	</table>
</div>
@endsection