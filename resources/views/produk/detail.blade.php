@extends('layouts.app')

@section('content')

<div class="cotainer">
  <div class="row" style="margin-right:0px;">
    <div class="col-sm-1"></div>
    <div class="col-sm-6">
      <h1> {{$barang->nama}} </h1></br>

      <table class="table">
        <thead>
          <tr>
            <th>Informasi Produk</th>
            <th> </th>
            <th> </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Berat</td>
            <td>{{ $barang->beratkg() }} kg</td>
            <td></td>
          </tr>
          <tr>
            <td>Stok</td>
            <td>{{ $barang->stok }}</td>
            <td></td>
          </tr>
          <tr>
            <td><strong>Deskripsi</strong></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="3" class="info">{{ $barang->deskripsi }}</td>
          </tr>
        </tbody>
      </table>
      <form action="{{ route('addtocart', ['barang' => $barang->id]) }}" method="POST">
        {{ csrf_field() }}
        <h2 class="harga">{{ $barang->hargarupiah() }}</h2>
        <div class="form-horizontal form-group">
          <label>Jumlah Beli : </label><input type="number" style="margin-left:20px;display:inline;width:100px;" class="form-control" name="jumlah">
        </div>
        <button type="submit" class="btn btn-success">Beli</button>
      </form>
    </div>
    <div class="col-sm-1"></div>
    <div class="col-sm-3">
      <br><br><br>
        <a href="#" data-toggle="modal" data-target="#preview"><img src="{{ asset('img/produk/'.$barang->nama_gambar) }}" width="100%"><p style="text-align:center;">Click to Preview</p></a></div>
      <div class="col-sm-1"></div>
    </div>
  </div>

  <div id="preview" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <img src="{{ asset('img/produk/'.$barang->nama_gambar) }}" width="100%">
      </div>
    </div>
  </div>
</div>
  @endsection

@section('css')
<style>
  .img-preview {
    padding:10px;
    border:solid 1px #ddd;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    margin-top:20px;
  }
</style>
@endsection