@component('mail::message')
# Bukti Bayar telah diterima

bukti pembayaran untuk transaksi:
id : {{ $troli->id }}
pembeli : {{ $troli->user->nama }}

silahkan cek halaman admin anda untuk meneruskan proses transaksi tersebut

berikut lampiran foto bukti transaksi yang telah dikirimkan.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
