@component('mail::message')
# <font color="red">Transaksi anda gagal</font>

 Maaf pembelian yang anda lakukan tidak dapat kami proses,<br> 
## Karena:

@component('mail::panel')
{{ $troli->keterangan }}.
@endcomponent

@component('mail::button', ['url' => url('/')])
Visit KasoCraft
@endcomponent

Mohon maaf atas ketidaknyamanannya

Thanks,<br>
Admin - {{ config('app.name') }}
@endcomponent
