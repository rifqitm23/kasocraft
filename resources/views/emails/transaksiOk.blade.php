@component('mail::message')
# <font color="green">Transaksi anda Berhasil</font>
<br> 
@component('mail::panel')
{{ $troli->keterangan }}.
@endcomponent

@component('mail::table')
| Produk       | berat/Harga   | Total  |
| :------------: | :-------------: | :--------:|
@foreach($barangs as $barang)
| {{ $barang->nama }} | {{ $barang->beratkg() }}/{{ $barang->hargarupiah() }} | {{ $barang->totalRp() }} |
@endforeach
|              | <b>Total Harga</b> | <b>{{ $troli->totalHarga() }}</b> |
@endcomponent

@component('mail::button', ['url' => url('/')])
Visit KasoCraft
@endcomponent

Terima kasih atas kepercayaan anda, selamat menikmati.

Thanks,<br>
Admin - {{ config('app.name') }}
@endcomponent
