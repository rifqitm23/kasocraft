@component('mail::message')
# Terima kasih telah melakukan pembelian, 
## untuk melanjutkan proses transaksi silahkan:

@component('mail::panel')
1. melakukan pembayaran sejumlah
<b>{{ $troli->totalHarga() }}</b>
ke salah satu Rekening:
<br>
a. Mandiri : <b>019283721398</b>
A/N KasoCraft<br>
b. BCA : <b>687908126789</b>
A/N KasoCraft
2. Mengisi Form Konfirmasi pembayaran pada halaman transaksi anda atau melalui tombol konfirmasi pembayaran

@component('mail::button', ['url' => url('/transaksi/'.$troli->id)])
Konfirmasi Pembayaran
@endcomponent

@endcomponent

@component('mail::table')
| Produk       | berat/Harga   | jumlah | Total  |
| :------------: | :-------------: | :---: |:--------:|
@foreach($troli->barangs()->get() as $barang)
| {{ $barang->nama }} | {{ $barang->beratkg() }}/{{ $barang->hargarupiah() }} | {{ $barang->pivot->qty }}|{{ $barang->totalRp() }} |
@endforeach
|              | | <b>Total Harga</b> | <b>{{ $troli->totalHarga() }}</b> |
@endcomponent

Terima kasih,<br>
{{ config('app.name') }}
@endcomponent
