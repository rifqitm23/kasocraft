@extends('layouts.app')

@section('css')
	<style>
		#myCarousel {
			margin-bottom:25px;
		}
	</style>
@endsection

@section('content')
	<!--CAROUSEL -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      	<img src="{{ asset('img/carousel/foto.jpg') }}" width="100%">
    </div>

    <div class="item">
      	<img src="{{ asset('img/carousel/promo.jpg') }}" width="100%">
    </div>

    <div class="item">
      	<img src="{{ asset('img/carousel/promo2.jpg') }}" width="100%">
    </div>

    <div class="item">
      	<img src="{{ asset('img/carousel/promo3.jpg') }}" width="100%">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <!-- <span class="glyphicon glyphicon-chevron-left"></span> -->
    <span class="icon-prev"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="icon-next"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<div class="container-fluid">
	<div class="row text-center">
		<img src="{{ asset('img/home/bl.png') }}">
	</div>

	<div class="row text-center">
		
		@foreach ($kategori as $item)
		<div class="col-xs-6 col-md-4">
			<div class="card">
				<a href="{{ url('/produk/kategori/'.$item->slug) }}"><img src="{{ asset('img/home/'.$item->slug.'.jpg') }}" width="100%"></a>
				<div class="card-title">
					<h2>{{ $item->nama }}</h2>
					<a href="{{ url('/produk/kategori/'.$item->slug) }}" class="card-btn">Lihat</a>
				</div>
			</div>
		</div>
    @endforeach
	
	</div>
</div>


@endsection