@extends('layouts.app')

@section('content')

<div class="container">
	<h1 align="center">Desa Wisata Kasongan</h1></br>
	<center>
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1523.8813186459683!2d110.34408882327018!3d-7.846885141528456!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a562f4730fbad%3A0x51f14d0f4ca890c!2sDesa+Wisata+Kasongan!5e0!3m2!1sid!2sid!4v1512200225858" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</center>

	<p align="center">
		Mempermudah pengrajin dan pembeli dalam bertransaksi<br>kasocraft menjual berbagai kerajinan yang dibuat di desa Kasongan<br>Bertujuan untuk menjaga dan mengembangkan kesenian khususnya dalam pembuatan gerabah
	</p>
	<p align="center">
		<img src="{{ asset('img/tentang/dr.png') }}" width="30%">
	</p>
</div>

@endsection